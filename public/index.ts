const playtile = document.getElementById("root") as HTMLDivElement;

const size = 4;
const swipeThreshold = 30;

const randomCoord = () => Math.round(Math.random() * 3) + 1;
const randomTileNumber = () => [2, 4][Math.round(Math.random())];
const doubleStringValue = (a: string) => (parseInt(a) * 2).toString();

// Credits: https://github.com/daniel-huang-1230/Game-2048/blob/master/Gui2048.java#L50
const colors = {
	2: "rgb(238, 228, 218)",
	4: "rgb(237, 224, 200)",
	8: "rgb(242, 177, 121)",
	16: "rgb(245, 149, 99)",
	32: "rgb(246, 124, 95)",
	64: "rgb(246, 94, 59)",
	128: "rgb(237, 207, 114)",
	256: "rgb(237, 204, 97)",
	512: "rgb(237, 200, 80)",
	1024: "rgb(237, 197, 63)",
	2048: "rgb(237, 194, 46)",
	light: "rgb(249, 246, 242)",
	dark: "rgb(119, 110, 101)",
};

let existingTileIds: Set<string> = new Set<string>();
let moved = false;

document.addEventListener("keyup", (e) => {
	if (gameEnded) {
		return;
	}

	if (
		["w", "a", "s", "d", "ArrowUp", "ArrowRight", "ArrowLeft", "ArrowDown"].includes(
			e.key
		)
	) {
		moveTiles(e.key);
	}
});

//#region Movement

const moveTiles = (key: string) => {
	let tiles = Array.from(playtile.querySelectorAll(".tile")).sort((a, b) =>
		a.id.localeCompare(b.id)
	);

	// Sort the elements so they are moved in the rigth order
	if (key === "d" || key === "s" || key === "ArrowRight" || key === "ArrowDown") {
		tiles = tiles.reverse();
	}

	tiles.forEach((div) => {
		moved = false;
		moveElement(div as HTMLElement, key);
		tiles = Array.from(playtile.querySelectorAll(".tile"));
	});

	if (moved) {
		generateElement();
	} else if (existingTileIds.size === 16) {
		if (!isGameStillPlayable(tiles)) {
			endGame("You lost");
		}
	}
};

const moveElement = (tile: HTMLElement, pressedKey: string) => {
	let x = parseInt(tile.style.gridColumn);
	let y = parseInt(tile.style.gridRow);

	let shouldMove: boolean;

	// TODO: DRY
	switch (pressedKey) {
		case "ArrowRight":
		case "d":
			for (let nextX = x + 1; nextX <= size; nextX++) {
				let el = document.getElementById(`${nextX};${y}`);

				if (el === null) {
					shouldMove = true;
					x = nextX;
					continue;
				}

				if (el !== null && el.innerHTML !== tile.innerHTML) {
					break;
				}

				if (el.innerHTML === tile.innerHTML) {
					mergeElements(el, tile);
					return;
				}
			}
			break;

		case "ArrowLeft":
		case "a":
			for (let prevX = x - 1; 1 <= prevX; prevX--) {
				let el = document.getElementById(`${prevX};${y}`);

				if (el === null) {
					shouldMove = true;
					x = prevX;
					continue;
				}

				if (el !== null && el.innerHTML !== tile.innerHTML) {
					// Element is next to element with diferent number, we cannot move nor merge them
					break;
				}

				if (el.innerHTML === tile.innerHTML) {
					mergeElements(el, tile);
					return;
				}
			}
			break;

		case "ArrowUp":
		case "w":
			for (let prevY = y - 1; 1 <= prevY; prevY--) {
				let el = document.getElementById(`${x};${prevY}`);

				if (el === null) {
					shouldMove = true;
					y = prevY;
					continue;
				}

				if (el !== null && el.innerHTML !== tile.innerHTML) {
					// Element is next to element with diferent number, we cannot move nor merge them
					break;
				}

				if (el.innerHTML === tile.innerHTML) {
					mergeElements(el, tile);
					return;
				}
			}
			break;

		case "ArrowDown":
		case "s":
			for (let nextY = y + 1; nextY <= size; nextY++) {
				let el = document.getElementById(`${x};${nextY}`);

				if (el === null) {
					shouldMove = true;
					y = nextY;
					continue;
				}

				if (el !== null && el.innerHTML !== tile.innerHTML) {
					// Element is next to element with diferent number, we cannot move nor merge them
					break;
				}

				if (el.innerHTML === tile.innerHTML) {
					mergeElements(el, tile);
					return;
				}
			}
			break;

		default:
			console.error(`Invalid key pressed: '${pressedKey}'`);
			return;
	}

	if (shouldMove) {
		moveElementTo(tile, x, y);
	}
};

const moveElementTo = (element: HTMLElement, x: Number | string, y: Number | string) => {
	existingTileIds.delete(element.id);
	element.style.gridColumn = x.toString();
	element.style.gridRow = y.toString();
	element.id = `${x};${y}`;
	moved = true;
	existingTileIds.add(element.id);
};

const mergeElements = (mergeTo: HTMLElement, mergeFrom: HTMLElement) => {
	existingTileIds.delete(mergeFrom.id);
	playtile.removeChild(mergeFrom);
	mergeTo.innerHTML = doubleStringValue(mergeTo.innerHTML);
	mergeTo.style.backgroundColor = colors[mergeTo.innerHTML];

	if (parseInt(mergeTo.innerHTML) <= 8) {
		mergeTo.style.color = colors["dark"];
	} else {
		mergeTo.style.color = colors["light"];
	}

	if (mergeTo.innerHTML === "2048") {
		endGame("You won!");
	}
	moved = true;
};

const generateElement = () => {
	let x: Number, y: Number;

	//TODO: Find empty tiles to place a new one - the won't be an infinite loop when the playtile is full
	do {
		x = randomCoord();
		y = randomCoord();
	} while (existingTileIds.has(`${x};${y}`));

	let node = document.createElement("div");
	node.className = "tile";
	node.style.gridRow = y.toString();
	node.style.gridColumn = x.toString();
	node.id = `${x};${y}`;
	node.innerHTML = randomTileNumber().toString();
	node.style.backgroundColor = colors[node.innerHTML];

	if (parseInt(node.innerHTML) <= 8) {
		node.style.color = colors["dark"];
	} else {
		node.style.color = colors["light"];
	}

	existingTileIds.add(node.id);
	playtile.appendChild(node);
};

//#endregion

//#region GameState

let modalBtn = document.getElementById("modalBtn") as HTMLButtonElement;
let modalTxt = document.getElementById("modalMsg");
let gameEnded = false;

const startGame = () => {
	generateElement();
	generateElement();
	gameEnded = false;
};

const isGameStillPlayable = (tiles: Array<Element>) =>
	tiles.some((t) => {
		let x = parseInt(t.id[0]);
		let y = parseInt(t.id[2]);

		// Check if tile above or next to `t` has the same number meaning it's still playable
		let neighbors = Array.from(
			document.querySelectorAll(
				`.tile[id="${x - 1};${y}"], .tile[id="${x + 1};${y}"], .tile[id="${x};${
					y - 1
				}"], .tile[id="${x};${y + 1}"]`
			)
		);

		return neighbors.some((n) => n.innerHTML == t.innerHTML);
	});

const endGame = (message: string) => {
	gameEnded = true;
	modalTxt.innerHTML = message;
	modalBtn.click();
};

document.getElementById("newGameBtn").addEventListener("click", () => {
	playtile.textContent = "";
	existingTileIds.clear();
	startGame();
});

//#endregion

//#region Scrolling

let initialX = null;
let initialY = null;

const startTouch = (e: TouchEvent) => {
	initialX = e.touches[0].clientX;
	initialY = e.touches[0].clientY;
};

const moveTouch = (e: TouchEvent) => {
	if (gameEnded || initialX === null || initialY === null) {
		return;
	}

	let currentX = e.touches[0].clientX;
	let currentY = e.touches[0].clientY;

	let diffX = initialX - currentX;
	let diffY = initialY - currentY;

	if (Math.abs(diffX) < swipeThreshold && Math.abs(diffY) < swipeThreshold) {
		return;
	}

	// Horizontal
	if (Math.abs(diffX) > Math.abs(diffY)) {
		// Left
		if (diffX > 0) {
			moveTiles("a");
		}
		// Right
		else {
			moveTiles("d");
		}
	}
	// Vertical
	else {
		// Up
		if (diffY > 0) {
			moveTiles("w");
		}
		// Down
		else {
			moveTiles("s");
		}
	}

	initialX = null;
	initialY = null;

	e.preventDefault();
};

document.addEventListener("touchstart", startTouch);
document.addEventListener("touchmove", moveTouch);

//#endregion

startGame();
